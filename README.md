# README #

There is API Service provided data in required format.

### How it works? ###

It consists of two parts: Refresher and API.
API provided cached data from storage.
Storage part contains two DBs: one for read, second for refresh. Refresher microservice is responsible for update dbs.
When first DB has filled by data it would be used as DB for reading, the second DB - for writing.
In this way API always provides prepared data, microservice is responsible for updating DBs.

### How do I get set up? ###

Refresher (<refresher>)
	https://localhost:5001
	https://tvmazerefresher.azurewebsites.net	
Scrap API (<scraper>)
	https://localhost:44304
	https://tvmazescraper.azurewebsites.net
	
	
1. On launch Refresher locally it will start refresh. (for locally started in Debug it will receive first 250 shows(1 TvMaze page))
	To manually start refresh: <refresher>/api/refresh/restart  (refresh expiring time set up in Refresher.appsettings.json. Current value 20 minutes)
	To check the refresh status: <refresher>/api/refresh/status
	To check the current writing context: <scraper>/api/writecontext
2. On refresh complete writing context will change and TvMazeScraper will read from just refreshed db.
	To get shows <scraper>/api/shows or <scraper>/api/shows?page=
	