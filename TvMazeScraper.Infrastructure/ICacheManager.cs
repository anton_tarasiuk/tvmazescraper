﻿using System;
using System.Collections.Generic;
using System.Text;
using TvMazeScraper.Models;
using TvMazeScraper.Models.Models;

namespace TvMazeScraper.Infrastructure
{
    public interface ICacheManager
    {
		MazeShowPage CachePage(int pageNumber, IEnumerable<MazeShow> shows);
		MazeShowPage GetCachedPage(int pageNumber);
		void CompleteRefresh();
	}
}
