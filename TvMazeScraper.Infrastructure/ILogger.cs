﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TvMazeScraper.Infrastructure
{
    public interface ILogger
    {
		void Debug(string message);
		void Error(string message);
	}
}
