﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TvMazeScraper.Infrastructure
{
    public interface IHttpHelper
    {
		Task<string> Load(string requestUri);
    }
}
