﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TvMazeScraper.Infrastructure
{
	public class HttpHelper : IHttpHelper
	{
		private const string _userAgentName = "User-Agent";
		private const string _userAgentValue = "MazeApiScraper";

		public async Task<string> Load(string uri)
		{
			string response;
			using (var httpClient = new HttpClient())
			{
				httpClient.DefaultRequestHeaders.Accept.Clear();
				httpClient.DefaultRequestHeaders.Add(_userAgentName, _userAgentValue);
				response = await httpClient.GetStringAsync(uri);
			}

			return response;
		}
	}
}
