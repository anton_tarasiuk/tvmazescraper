﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TvMazeScraper.Cache
{
    public interface IRefreshService
    {
		Task RefreshData();
    }
}
