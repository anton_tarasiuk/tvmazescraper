﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TvMazeScraper.Models
{
	public class BaseEntity
	{
		[JsonProperty("Id")]
		public int Id { get; set; }
		[JsonProperty("Name")]
		public string Name { get; set; }
	}
}
