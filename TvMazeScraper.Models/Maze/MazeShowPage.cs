﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TvMazeScraper.Models.Scrap;

namespace TvMazeScraper.Models
{
	public class MazeShowPage
	{
		private IEnumerable<MazeShow> shows;

		public MazeShowPage(int pageNumber, IEnumerable<MazeShow> shows)
		{
			PageNumber = pageNumber;
			Shows = shows;
		}
		public MazeShowPage()
		{

		}

		public int Id { get; set; }
		public int PageNumber { get; set; }
		public IEnumerable<MazeShow> Shows { get; set; }
    }
}
