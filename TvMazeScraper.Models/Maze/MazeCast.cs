﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TvMazeScraper.Models.Models
{
    public class MazeCast
    {
		public int Id { get; set; }
		[JsonIgnore]
		public int ShowId { get; set; }
		public MazePerson Person { get; set; }
    }
}
