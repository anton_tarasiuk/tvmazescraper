﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TvMazeScraper.Models.Models;

namespace TvMazeScraper.Models
{
    public class MazeShow : BaseEntity
    {
		[JsonIgnore]
		public IEnumerable<MazeCast> Casts { get; set; }
    }
}
