﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace TvMazeScraper.Models
{
	public class MazePerson : BaseEntity
	{
		public string Birthday { get; set; }
	}
}
