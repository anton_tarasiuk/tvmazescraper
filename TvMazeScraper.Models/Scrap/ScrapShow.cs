﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TvMazeScraper.Models.Models;

namespace TvMazeScraper.Models.Scrap
{
    public class ScrapShow
    {
		[JsonProperty("Id")]
		public int Id { get; set; }
		[JsonProperty("Name")]
		public string Name { get; set; }
		[JsonProperty("cast")]
		public IEnumerable<MazePerson> Cast { get; set; }
		public ScrapShow(MazeShow show)
		{
			Id = show.Id;
			Name = show.Name;
			Cast = show.Casts.Select(cast => cast.Person);
		}
	}
}
