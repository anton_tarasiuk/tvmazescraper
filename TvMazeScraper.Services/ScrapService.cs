﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TvMazeScraper.Models;
using TvMazeScraper.Infrastructure;
using TvMazeScraper.Models.Scrap;
using System.Linq;
using TvMazeScraper.Cache;

namespace TvMazeScraper.Services
{
	public class ScrapService : IScrapService
	{
		private const int _mazeApiPageLimit = 250;
		private const int _scrapApiPageLimit = 25;
				
		private readonly IMazeApiService _mazeApiService;
		private readonly ICacheManager _cacheManager;

		public ScrapService(IMazeApiService mazeApiService, ICacheManager mazeCache, IRefreshService refreshService)
		{
			_mazeApiService = mazeApiService;
			_cacheManager = mazeCache;
		}

		public IEnumerable<ScrapShow> GetShows(int page)
		{
			//in mazeApi 250 shows on page
			//scrapApi would be 25 on page
			double requiredShowsCount = (page + 1) * _scrapApiPageLimit;
			var mazePage = (int)Math.Floor(requiredShowsCount / _mazeApiPageLimit);

			var showsPage = _cacheManager.GetCachedPage(mazePage);
			if (showsPage == null)
				return null;

			var skipShows = (page % (_mazeApiPageLimit / _scrapApiPageLimit)) * _scrapApiPageLimit;
			var requiredShows = showsPage.Shows.Skip(skipShows).Take(_scrapApiPageLimit);

			return ConvertToScrapShows(requiredShows);
		}

		private IEnumerable<ScrapShow> ConvertToScrapShows(IEnumerable<MazeShow> mazeShows)
		{
			return mazeShows.Select(mShow => new ScrapShow(mShow));
		}
	}
}
