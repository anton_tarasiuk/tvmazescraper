﻿using TvMazeScraper.Infrastructure;

namespace TvMazeScraper.Services
{
	/// <summary>
	/// This logger can be replaced with any other.
	/// </summary>
	public class Logger : ILogger
	{

		public void Debug(string message)
		{
			System.Diagnostics.Debug.WriteLine($"DEBUG: {message}");
		}

		public void Error(string message)
		{
			System.Diagnostics.Debug.WriteLine($"ERROR: {message}");
		}
	}
}
