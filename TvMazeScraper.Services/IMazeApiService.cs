﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TvMazeScraper.Models;
using TvMazeScraper.Models.Models;

namespace TvMazeScraper.Infrastructure
{
    public interface IMazeApiService
    {
		Task<IEnumerable<MazeShow>> GetShowsByPage(int page);
		Task<IEnumerable<MazeCast>> GetCastByShowId(int showId);
    }
}
