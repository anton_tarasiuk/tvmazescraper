﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TvMazeScraper.Models;
using TvMazeScraper.Models.Models;

namespace TvMazeScraper.Infrastructure
{
	public class MazeApiService : IMazeApiService
	{
		private const string _baseUri = "http://api.tvmaze.com/shows{0}";
		private const string _pageUriParameter = "?page={0}";
		private const string _castUri = "/{0}/cast";

		private IHttpHelper _httpHelper;
		private readonly ILogger _logger;

		public MazeApiService(IHttpHelper httpHelper, ILogger logger)
		{
			_httpHelper = httpHelper;
			_logger = logger;
		}

		public async Task<IEnumerable<MazeShow>> GetShowsByPage(int page)
		{
			var pageUriParameter = string.Format(_pageUriParameter, page);
			var requestUri = string.Format(_baseUri, pageUriParameter);
			var response = await _httpHelper.Load(requestUri);

			try
			{
				var result = JsonConvert.DeserializeObject<IEnumerable<MazeShow>>(response);
				return result;
			}
			catch (Exception ex)
			{
				_logger.Error($"TvMazeScraper.Infrastructure.MazeApiService.GetShowsByPage Exception: {ex.Message}");
				return null;
			}
		}

		public async Task<IEnumerable<MazeCast>> GetCastByShowId(int showId)
		{
			var castUri = string.Format(_castUri, showId);
			var requestUri = string.Format(_baseUri, castUri);
			var response = await _httpHelper.Load(requestUri);
			try
			{
				var result = JsonConvert.DeserializeObject<List<MazeCast>>(response);
				return result;
			}
			catch (Exception ex)
			{
				_logger.Error($"TvMazeScraper.Infrastructure.MazeApiService.GetCastByShowId Exception: {ex.Message}");
				return null;
			}
			
		}
	}
}
