﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TvMazeScraper.Models.Scrap;

namespace TvMazeScraper.Services
{
    public interface IScrapService
    {
		IEnumerable<ScrapShow> GetShows(int page);
    }
}
