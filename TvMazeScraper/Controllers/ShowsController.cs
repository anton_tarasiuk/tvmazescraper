﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TvMazeScraper.Cache;
using TvMazeScraper.Models;
using TvMazeScraper.Models.Scrap;
using TvMazeScraper.Services;

namespace TvMazeScraper.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShowsController : ControllerBase
    {
		private readonly IScrapService _scrapService;
		private readonly IRefreshService _refreshService;

		public ShowsController(IScrapService scrapService, IRefreshService refreshService)
		{			
			_scrapService = scrapService;
			_refreshService = refreshService;
		}

		// GET api/shows?page=2
		[HttpGet]
        public ActionResult Get([FromQuery]int page = 0)
        {
			var shows = _scrapService.GetShows(page);

			if (shows == null || !shows.Any())
				return NoContent();

			return Ok(shows);
        }
	}
}
