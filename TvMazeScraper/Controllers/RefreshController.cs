﻿using Microsoft.AspNetCore.Mvc;
using TvMazeScraper.Cache;

namespace TvMazeScraper.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RefreshController : ControllerBase
    {
		private readonly IRefreshService _refreshService;

		public RefreshController(IRefreshService refreshService)
		{			
			_refreshService = refreshService;
		}

		/// <summary>
		///use to start Refresh One of DBs(InMemory) as an example instead of real Microservice which would refresh real DBs. 
		/// </summary>
		//GET api/refresh
		[HttpGet]
		public ActionResult Refresh()
		{
			_refreshService.RefreshData();
			return Content("Refresh started. Wait a bit while data received.");
		}
	}
}
