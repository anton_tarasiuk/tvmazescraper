﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TvMazeScraper.Cache;
using TvMazeScraper.Infrastructure;
using TvMazeScraper.Models.Models;
using TvMazeScraper.Services;

namespace TvMazeScraper
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
			services.AddDbContext<MazeContextA>(opt =>
				opt.UseInMemoryDatabase("MazeShowsA"), ServiceLifetime.Singleton);
			services.AddDbContext<MazeContextB>(opt =>
				opt.UseInMemoryDatabase("MazeShowsB"), ServiceLifetime.Singleton);

			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

			RegisterAdditionalServices(services);
		}

		private void RegisterAdditionalServices(IServiceCollection services)
		{
			services.AddSingleton<Infrastructure.ILogger, Logger>();
			services.AddSingleton<IHttpHelper, HttpHelper>();
			services.AddSingleton<IMazeApiService, MazeApiService>();
			services.AddSingleton<ICacheManager, CacheManager>();
			services.AddSingleton<IScrapService, ScrapService>();
			services.AddSingleton<IRefreshService, RefreshService>();			
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
