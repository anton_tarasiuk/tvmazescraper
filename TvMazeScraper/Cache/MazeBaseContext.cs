﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TvMazeScraper.Models;
using TvMazeScraper.Models.Models;

namespace TvMazeScraper
{
	public class MazeBaseContext : DbContext
	{
		private bool _isUpToDate;

		public MazeBaseContext(DbContextOptions options)
			: base(options)
		{
		}

		public bool IsUpToDate { get => _isUpToDate; set { _isUpToDate = value; } }

		public DbSet<MazeShowPage> ShowPages { get; set; }
		public DbSet<MazeCast> ShowCast { get; set; }
	}
}
