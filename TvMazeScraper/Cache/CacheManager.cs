﻿using System.Collections.Generic;
using System.Linq;
using TvMazeScraper.Infrastructure;
using TvMazeScraper.Models;

namespace TvMazeScraper.Cache
{
	public class CacheManager : ICacheManager
	{
		private readonly MazeContextA _contextA;
		private readonly MazeContextB _contextB;

		public MazeBaseContext ReadContext { get;set; }
		public MazeBaseContext WriteContext { get;set; }

		public CacheManager(MazeContextA contextA, MazeContextB contextB)
		{
			_contextA = contextA;
			_contextA.IsUpToDate = false;
			_contextB = contextB;
			_contextB.IsUpToDate = false;
			WriteContext = _contextA;
			ReadContext = _contextB;
		}

		public MazeShowPage CachePage(int pageNumber, IEnumerable<MazeShow> shows)
		{
			var showPage = new MazeShowPage(pageNumber, shows);

			WriteContext.ShowPages.Add(showPage);
			WriteContext.SaveChanges();
			return showPage;
		}

		public MazeShowPage GetCachedPage(int pageNumber)
		{
			if (!ReadContext.IsUpToDate)
				return null;

			return ReadContext.ShowPages.FirstOrDefault(sp => sp.PageNumber == pageNumber);
		}

		public void CompleteRefresh()
		{
			WriteContext.IsUpToDate = true;
			var dbToRead = WriteContext;
			
			ReadContext.IsUpToDate = false;
			var dbToWrite = ReadContext;

			WriteContext = dbToWrite;
			ReadContext = dbToRead;
		}
	}
}
