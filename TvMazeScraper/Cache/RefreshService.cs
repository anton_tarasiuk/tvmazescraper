﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TvMazeScraper.Infrastructure;
using TvMazeScraper.Models;
using TvMazeScraper.Models.Models;
using TvMazeScraper.Services;

namespace TvMazeScraper.Cache
{
	public class RefreshService : IRefreshService
	{
		private readonly IMazeApiService _mazeApiService;
		private readonly ICacheManager _cacheManager;
		private readonly ILogger _logger;

#if DEBUG
		//for test purposes get only 2 pages. All pages count 150.
		private int _debugPagesLimit = 2;
#endif

		public RefreshService(IMazeApiService mazeApiService, ICacheManager mazeCache, ILogger logger)
		{
			_mazeApiService = mazeApiService;
			_cacheManager = mazeCache;
			_logger = logger;
		}

		public async Task RefreshData()
		{
			int page = 0;
			var shows = (await _mazeApiService.GetShowsByPage(page)).ToList();

			try
			{
				while (shows != null)
				{
#if DEBUG
					if (page == _debugPagesLimit)
						break;
#endif
					for (int i = 0; i < shows.Count(); i++)
					{
						var show = shows.ElementAt(i);

						IEnumerable<MazeCast> showCast = null;
						try
						{
							showCast = await _mazeApiService.GetCastByShowId(show.Id);
						}
						catch (Exception ex)
						{
							_logger.Debug($"Show.Id={show.Id}; GetCast Exception: {ex.Message}");
							await Task.Delay(5000);
							i--;
							continue;
						}

						if (showCast != null)
						{
							show.Casts = showCast;
							_logger.Debug($"Show.Id={show.Id}; Cast.Count={showCast.Count()}");
						}
						else
							_logger.Debug($"Show.Id={show.Id}; ERROR: Cast=null");
					}

					_cacheManager.CachePage(page, shows);
					page++;
					shows = (await _mazeApiService.GetShowsByPage(page)).ToList();
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				_logger.Debug($"There is no data for requested page# {page-1}");
			}
			finally
			{
				_logger.Debug($"Refresh Completed. Last saved Show.Id={shows.Last().Id}, Maze Page={page-1}. Received {page} pages.");
				_cacheManager.CompleteRefresh();
			}
		}
	}
}
